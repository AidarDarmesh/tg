<?php

namespace App\Http\TelegramCommands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class StartCommand extends Command
{
    protected $name = "start";

    protected $description = "Start Command to get you started";

    public function handle($args)
    {

        $this->replyWithMessage(['text' => 'Привет! Я бот qagaz daiyn. Отправь мне файл на распечатку']);

    }
}