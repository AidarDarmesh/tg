<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/bot', function(){

	$response = Telegram::getMe();

	$botId = $response->getId();
	$first_name = $response->getFirstName();
	$username = $response->getUsername();

	return $response;

});

Route::post('/Tf4cXKhYmhtaG7se/webhook', 'TelegramController@index');

// Route::get('/setWebhook', function(){

// 	$response = Telegram::setWebhook([

// 		'url' => 'https://daiyn.com/Tf4cXKhYmhtaG7se/webhook',
// 		'certificate' => '/etc/letsencrypt/live/daiyn.com/privkey.pem'

// 	]);

// });